import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './containers/home';
import Order from './containers/order';
import AddProduct from './containers/addProduct';
import Header from './components/header';
import './index.less';
import 'normalize.css/normalize.css';

const App = () => {
  return (
    <Router>
      <Header/>
      <main className="app-content">
        <section className="page-content">
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/order' component={Order}/>
            <Route path='/products/add' component={AddProduct}/>
          </Switch>
        </section>
      </main>
    </Router>
  )
};

export default App;