import React from 'react';
import { NavLink } from 'react-router-dom';
import './style.less';

const Header = () => {
  return (
    <header className='shop-header'>
      <nav>
        <NavLink exact activeClassName='link-active' to='/'>商城</NavLink>
        <NavLink activeClassName='link-active' to='/order'>订单</NavLink>
        <NavLink activeClassName='link-active' to='/products/add'>添加商品</NavLink>
      </nav>
    </header>
  )
};

export default Header;