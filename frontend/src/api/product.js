const BASE_URL = 'http://localhost:8080/api/products';

export const fetchProducts = () => {
  return fetch(BASE_URL).then(response => response.json())
};