import api from '../../../api';
import * as types from './types';

export const fetchProducts = () => dispatch => {
  api.fetchProducts().then(result => {
    dispatch({ type: types.FETCH_PRODUCT_LIST, products: result})
  })
};