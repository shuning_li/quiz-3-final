import * as types from './types';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_PRODUCT_LIST:
      return action['products'];
    default:
      return state;
  }
};