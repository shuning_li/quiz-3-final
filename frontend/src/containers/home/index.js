import React from 'react';
import { connect } from 'react-redux';
import { fetchProducts } from './reducers/actions';
import './style.less';

class Home extends React.Component{

  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    const { products } = this.props;
    console.log(products);
    return (
      <main className='home-content'>
        {products.map(item => (
          <div className="product-item" key={item['id']}>
            <div className="image-wrapper">
              <img src={item['imageUrl']} alt=""/>
            </div>
            <p className="product-name">{item['name']}</p>
            <p className="product-price">单价:{item['price']}/{item['unit']}</p>
          </div>
        ))}
      </main>
    )
  }
}

export default connect(state => ({
  products: state.products
}), {
  fetchProducts
})(Home);