import { combineReducers } from 'redux';
import productsReducer from '../containers/home/reducers/productsReducer';

export default combineReducers({
  products: productsReducer
});