package com.twuc.backend.controller;

import com.twuc.backend.contract.CreateOrderRequest;
import com.twuc.backend.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private ProductRepository productRepository;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrder(@RequestBody CreateOrderRequest createOrderRequest) {
        Map<Long, Integer> map = createOrderRequest.getMap();
        map.entrySet().forEach(entry -> {
            Long productId = entry.getKey();
            Integer num = entry.getValue();
            Product product = productRepository.getOne(productId);

            Order order = new Order();
            OrderItem orderItem = new OrderItem(num);

            orderItem.setProduct(product);
            order.addOrderItem(orderItem);

            orderRepository.save(order);
            orderItemRepository.save(orderItem);
        });
    }
}
