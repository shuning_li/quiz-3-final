package com.twuc.backend.controller;

import com.twuc.backend.contract.CreateProductRequest;
import com.twuc.backend.domain.Product;
import com.twuc.backend.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping("")
    public ResponseEntity createProduct(@Valid @RequestBody CreateProductRequest createProductRequest) {
        String name = createProductRequest.getName();
        double price = createProductRequest.getPrice();
        String unit = createProductRequest.getUnit();
        String imageUrl = createProductRequest.getImageUrl();

        Product product = new Product(name, price, unit, imageUrl);

        productRepository.save(product);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/products/" + product.getId())
                .build();
    }

    @GetMapping("")
    public List<Product> getProducts() {
        List<Product> products = productRepository.findAll();
        return products;
    }
}
