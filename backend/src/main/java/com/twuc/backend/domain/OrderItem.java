package com.twuc.backend.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "order_items")
public class OrderItem {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Integer num;
    @OneToOne
    private Product product;
    @ManyToOne
    private Order order;

    public OrderItem() {
    }

    public OrderItem(Integer num) {
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public Integer getNum() {
        return num;
    }

    public Product getProduct() {
        return product;
    }

    public Order getOrder() {
        return order;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(product, orderItem.product) &&
                Objects.equals(order, orderItem.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, order);
    }
}
