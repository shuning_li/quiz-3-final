package com.twuc.backend.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 128)
    private String name;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false, length = 32)
    private String unit;
    @Column(nullable = false, length = 256)
    private String imageUrl;
    @OneToOne(mappedBy = "product")
    private OrderItem orderItem;

    public Product() {
    }

    public Product(String name, Double price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
