package com.twuc.backend.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateProductRequest {
    @NotNull
    @Size(min = 1, max = 128)
    private String name;
    @NotNull
    private double price;
    @NotNull
    @Size(min = 1, max = 64)
    private String unit;
    @NotNull
    @Size(min = 1)
    private String imageUrl;

    public CreateProductRequest() {
    }

    public CreateProductRequest(String name, double price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
