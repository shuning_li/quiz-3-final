package com.twuc.backend.contract;

import java.util.Map;

public class CreateOrderRequest {
    private Map<Long, Integer> map;

    public CreateOrderRequest(Map<Long, Integer> map) {
        this.map = map;
    }

    public CreateOrderRequest() {
    }

    public Map<Long, Integer> getMap() {
        return map;
    }
}
