package com.twuc.backend;

import com.twuc.backend.contract.CreateProductRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest extends ApiTestBase {
    @BeforeEach
    void setUp() throws Exception {
        createProduct(new CreateProductRequest("可乐1", 2.50, "瓶", "xxx.jpg"));
        createProduct(new CreateProductRequest("可乐2", 2.50, "瓶", "xxx.jpg"));
        createProduct(new CreateProductRequest("可乐3", 2.50, "瓶", "xxx.jpg"));
    }

    @Disabled
    @Test
    void should_create_order() throws Exception {
        HashMap<Long, Integer> map = new HashMap<>();

        map.put(1l, 1);
        map.put(2l, 1);
        map.put(3l, 2);

        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(map)))
                .andExpect(status().isCreated());
    }

    private ResultActions createProduct(CreateProductRequest createProductRequest) throws Exception {
        return mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(createProductRequest)));
    }
}
