package com.twuc.backend;

import com.twuc.backend.domain.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OrderRepoTest {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;

    @Test
    void should_creat_and_save_order() {
        Order savedOrder = createAndSaveOrder();

        assertNotNull(savedOrder);
        assertEquals(1, savedOrder.getOrderItems().size());
        assertNotNull(savedOrder.getOrderItems().get(0).getProduct());
    }

    @Test
    void should_change_product_num() {
        Order saveOrder = createAndSaveOrder();
        saveOrder.getOrderItems().get(0).setNum(2);

        Order updateOrder = orderRepository.getOne(saveOrder.getId());

        assertEquals(Integer.valueOf(2), updateOrder.getOrderItems().get(0).getNum());
    }

    @Test
    void should_delete_product() {
        Order saveOrder = createAndSaveOrder();
        orderRepository.delete(saveOrder);

        Optional<Order> optionalOrder = orderRepository.findById(saveOrder.getId());

        assertFalse(optionalOrder.isPresent());
    }

    @Test
    void should_remove_product() {
        Order saveOrder = createAndSaveOrder();
        OrderItem newOrderItem = new OrderItem(2);
        saveOrder.addOrderItem(newOrderItem);
        saveOrder.removeOrderItem(saveOrder.getOrderItems().get(0));

        Order queryOrder = orderRepository.getOne(saveOrder.getId());

        assertEquals(1, queryOrder.getOrderItems().size());
        assertEquals(newOrderItem, queryOrder.getOrderItems().get(0));
    }

    private Order createAndSaveOrder() {
        Order order = new Order();
        Product product = new Product("cola", 2.50, "瓶", "xxx.png");
        OrderItem orderItem = new OrderItem(1);

        productRepository.saveAndFlush(product);
        orderItemRepository.saveAndFlush(orderItem);
        orderRepository.saveAndFlush(order);

        order.addOrderItem(orderItem);
        orderItem.setProduct(product);

        Order savedOrder = orderRepository.getOne(order.getId());

        return savedOrder;
    }
}
