package com.twuc.backend;

import com.twuc.backend.domain.Product;
import com.twuc.backend.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductRepoTest {

    @Autowired
    ProductRepository repo;

    @Test
    void should_create_and_save_product() {
        Product product = new Product("可乐", 2.50, "瓶", "xxx.jpg");
        repo.saveAndFlush(product);

        Product savedProduct = repo.getOne(product.getId());

        assertNotNull(savedProduct);
    }
}
