package com.twuc.backend;

import com.twuc.backend.contract.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest extends ApiTestBase {
    @Test
    void should_create_new_product() throws Exception {
        createProduct(new CreateProductRequest("可乐", 2.50, "瓶", "xxx.jpg"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/products/1"));
    }

    @Test
    void should_return_400_when_params_invalid() throws Exception {
        createProduct(new CreateProductRequest("", 2.50, "瓶", "xxx.jpg"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_product_list() throws Exception {
        createProduct(new CreateProductRequest("可乐", 2.50, "瓶", "xxx.jpg"));
        createProduct(new CreateProductRequest("可乐", 2.50, "瓶", "xxx.jpg"));
        createProduct(new CreateProductRequest("可乐", 2.50, "瓶", "xxx.jpg"));

        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)));
    }

    private ResultActions createProduct(CreateProductRequest createProductRequest) throws Exception {
        return mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(createProductRequest)));
    }
}
