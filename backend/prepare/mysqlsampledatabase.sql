/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shop`;

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    name VARCHAR(128) NOT NULL ,
    price DOUBLE(10, 2) NOT NULL ,
    unit VARCHAR(32) NOT NULL ,
    image_url VARCHAR(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products` (id, name, price, unit, image_url) VALUES
    (1, 'Apple AirPods 配充电盒 Apple蓝牙耳机', 1246.00, '个', 'https://img11.360buyimg.com/n7/jfs/t1/29861/36/11965/55492/5c9352f7E5302d7a8/dd359563dc751ca5.jpg'),
    (2, '拜亚动力/拜雅 (beyerdynamic) Xelento remote', 6499.00, '个', 'https://img12.360buyimg.com/n7/jfs/t25666/116/2300758382/77130/73aefffd/5bc84deeNa0961977.jpg');


DROP TABLE IF EXISTS  `orders`;

CREATE TABLE `orders` (
    id BIGINT NOT NULL AUTO_INCREMENT ,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
    id BIGINT NOT NULL AUTO_INCREMENT,
    num INTEGER NOT NULL ,
    order_id BIGINT,
    product_id BIGINT,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `order_items`
    ADD CONSTRAINT FKbioxgbv59vetrxe0ejfubep1w
        FOREIGN KEY (`order_id`)
            REFERENCES `orders` (`id`);

ALTER TABLE `order_items`
    ADD CONSTRAINT FKocimc7dtr037rh4ls4l95nlfi
        FOREIGN KEY (`product_id`)
            REFERENCES `products`(`id`);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;